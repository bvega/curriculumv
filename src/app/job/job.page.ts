import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { CurriculumVitae } from '../interfaces/curriculumVitae';
import { Enterprise } from '../interfaces/enterprise';

@Component({
  selector: 'app-job',
  templateUrl: './job.page.html',
  styleUrls: ['./job.page.scss'],
})
export class JobPage implements OnInit {

  cvitae: CurriculumVitae;

  enterpriseOne: Enterprise={
    name: "",
    function: "",
    position: "",
    time: ""
  }

  enterpriseTwo: Enterprise={
    name: "",
    function: "",
    position: "",
    time: ""
  }

  constructor(public router:Router,public route: ActivatedRoute) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params =>{
      if(this.router.getCurrentNavigation().extras.state){
        this.cvitae = this.router.getCurrentNavigation().extras.state.cv;
      }
    });
  }

  siguiente(){
    this.cvitae.enterprseOne=this.enterpriseOne;
    this.cvitae.enterpriseTwo=this.enterpriseTwo;
    let navigationExtras: NavigationExtras ={
      state: {
        cv: this.cvitae
      }
    }
    this.router.navigate(['education'],navigationExtras);
  }

  anterior(){
    this.router.navigate(['person']);
  }

}
