import { Router, NavigationExtras } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Person } from '../interfaces/person';
import { CurriculumVitae } from '../interfaces/curriculumVitae';
import { Enterprise } from '../interfaces/enterprise';
import { Institute } from '../interfaces/institute';
import { DatePipe } from '@angular/common';
@Component({
  selector: 'app-person',
  templateUrl: './person.page.html',
  styleUrls: ['./person.page.scss'],
})
export class PersonPage implements OnInit {

  date: Date;
  person: Person={
    id: "",
    name: "",
    address: "",
    date:"",
    genre: "",
    experience: "No relleno este campo ya que tiene experiencia laboral"
  }
  enterpriseOne: Enterprise={
    name: "",
    function: "",
    position: "",
    time: ""
  }

  enterpriseTwo: Enterprise={
    name: "",
    function: "",
    position: "",
    time: ""
  }
  instituteOne: Institute={
    university: "",
    Title: "",
    Level: "Tercer Nivel"
  }

  instituteTwo: Institute={
    university: "",
    Title: "",
    Level: "Cuarto Nivel"
  }
  curriculumVitae: CurriculumVitae={
    person: this.person,
    experience: "",
    enterprseOne: this.enterpriseOne,
    enterpriseTwo: this.enterpriseTwo,
    instituteOne: this.instituteOne,
    instituteTwo: this.instituteTwo
  }

  constructor(public router: Router,private datePipe: DatePipe) { }

  ngOnInit() {
  }

  siguiente(){
    this.curriculumVitae.person.date=this.datePipe.transform(this.date,"yyyy/mm/dd");
    let navigationExtras: NavigationExtras ={
      state: {
        cv: this.curriculumVitae
      }
    }
    if(this.person.experience =="si"){
      this.router.navigate(['job'],navigationExtras);
    }else{
      this.router.navigate(['experience'],navigationExtras);
    }
    
  }

  anterior(){
    this.router.navigate(['home']);
  }


  RadioGenreChangeEvent(event){
    this.person.genre=event.detail.value;
  }

  RadioExperienceChangeEvent(event){
    this.person.experience=event.detail.value;
  }
}
