export interface Person{
    id: String;
    name: String;
    address: String;
    date: String;
    genre: String;
    experience: String;
}