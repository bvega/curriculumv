import { Person } from '../interfaces/person';
import { Institute } from './institute';
import { Enterprise } from './enterprise';
export interface CurriculumVitae{

    person: Person,
    experience: String,
    enterprseOne: Enterprise,
    enterpriseTwo: Enterprise,
    instituteOne: Institute,
    instituteTwo: Institute,


}