import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { CurriculumVitae } from '../interfaces/curriculumVitae';

@Component({
  selector: 'app-info',
  templateUrl: './info.page.html',
  styleUrls: ['./info.page.scss'],
})
export class InfoPage implements OnInit {

  cvitae: CurriculumVitae;

  constructor(public router: Router, public route: ActivatedRoute) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params =>{
      if(this.router.getCurrentNavigation().extras.state){
        this.cvitae = this.router.getCurrentNavigation().extras.state.cv;
      }
    });
    if(this.cvitae.instituteTwo.university ==""){
      this.cvitae.instituteTwo.Level="";
    }
  }

  generar(){
    this.router.navigate(['home']);
  }
}
