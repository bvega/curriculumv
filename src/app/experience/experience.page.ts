import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { CurriculumVitae } from '../interfaces/curriculumVitae';

@Component({
  selector: 'app-experience',
  templateUrl: './experience.page.html',
  styleUrls: ['./experience.page.scss'],
})
export class ExperiencePage implements OnInit {


  cvitae: CurriculumVitae

  constructor(public router: Router, public route: ActivatedRoute) { }

  ngOnInit() {

    this.route.queryParams.subscribe(params =>{
      if(this.router.getCurrentNavigation().extras.state){
        this.cvitae = this.router.getCurrentNavigation().extras.state.cv;
      }
    });

  }

  siguiente(){
    let navigationExtras: NavigationExtras ={
      state: {
        cv: this.cvitae
      }
    }
    this.router.navigate(['education'],navigationExtras);
  }

  anterior(){
    this.router.navigate(['person']);
  }
}
