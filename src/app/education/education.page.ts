import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { CurriculumVitae } from '../interfaces/curriculumVitae';
import { Institute } from '../interfaces/institute';

@Component({
  selector: 'app-education',
  templateUrl: './education.page.html',
  styleUrls: ['./education.page.scss'],
})
export class EducationPage implements OnInit {

  cvitae: CurriculumVitae

  instituteOne: Institute={
    university: "",
    Title: "",
    Level: "Tercer Nivel"
  }

  instituteTwo: Institute={
    university: "",
    Title: "",
    Level: "Cuarto Nivel"
  }
  constructor(public router: Router, public route: ActivatedRoute) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params =>{
      if(this.router.getCurrentNavigation().extras.state){
        this.cvitae = this.router.getCurrentNavigation().extras.state.cv;
      }
    });
  }

  siguiente(){
    this.cvitae.instituteOne=this.instituteOne;
    this.cvitae.instituteTwo=this.instituteTwo;
    let navigationExtras: NavigationExtras ={
      state: {
        cv: this.cvitae
      }
    }
    console.log(this.cvitae.experience);
    this.router.navigate(['info'],navigationExtras);
  }

  anterior(){
    this.router.navigate(['job']);
  }

}
